# Wheel 0.25+ needed to install certain packages on CPython 3.5+
# like Pillow and psycopg2
# See http://bitly.com/wheel-building-fails-CPython-35
# Verified bug on Python 3.5.1
wheel==0.30.0


# Bleeding edge Django
django==1.11 # pyup: >=1.10,<1.11

# Configuration
django-environ==0.4.4

# Python-PostgreSQL Database Adapter
psycopg2==2.7.3.1

# Unicode slugification
awesome-slugify==1.6.5

# Time zones support
pytz==2017.2

django-extensions==1.9.6
django-oauth-toolkit==1.0.0
djangorestframework==3.7.1
gevent==1.2.2
gunicorn==19.7.1
bs4==0.0.1
python-twitter==3.3
feedparser==5.2.1
lxml==4.1.0
django-cors-headers==2.1.0

# CUSTOMS
