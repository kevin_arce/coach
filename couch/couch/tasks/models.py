from django.db import models


class TaskLog(models.Model):
    TYPES = (
        ('post', 'post'),
        ('podcast', 'podcast')
    )

    date = models.DateTimeField(auto_now_add=True)
    count = models.IntegerField()
    data_type = models.CharField(choices=TYPES, blank=False, max_length=20)
    tech = models.ForeignKey('scrapers.Tech')
