from django.core.management.base import BaseCommand
from couch.utils.podcats import update_podcasts


class Command(BaseCommand):

    def handle(self, *args, **options):
        update_podcasts.update_podcasts()
