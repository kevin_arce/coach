from django.db import models
from couch.managers.managers import FullDataManager


class Podcast(models.Model):
    title = models.TextField(blank=False)
    date = models.DateField(null=False, auto_now=False)
    data_url = models.TextField(blank=False)
    length = models.CharField(max_length=20, blank=False)
    tech = models.ForeignKey('scrapers.tech', null=False)
    source = models.CharField(max_length=150, blank=True)
    objects = FullDataManager()

    def __str__(self):
        return self.title


class PodcastURL(models.Model):
    url = models.TextField(blank=False, unique=True)
    tech = models.ForeignKey('scrapers.tech', null=False)
    objects = FullDataManager()

    def save(self, *args, **kwargs):
        from utils.podcats.adapter import FeedAdapter
        adapter = FeedAdapter(url=self.url, tech=self.tech)
        adapter.save_to_bd()
        super(PodcastURL, self).save(*args, **kwargs)

    def __str__(self):
        return self.url
