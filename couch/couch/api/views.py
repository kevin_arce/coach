from couch.scrapers.models import Tech
from couch.profiles.models import Profile
from .serializers import (
    PostTimeSerializer, PodcastTimeSerializer, TechSerializer, UserProfileSerializer,
    GenericPostSerializer, GenericPodcastSerializer,
)

from django.contrib.auth import get_user_model

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

User = get_user_model()


class UserPosts(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        source = request.GET.get('source')
        profile = request.user.profile
        posts = profile.get_posts(source=source)
        data = PostTimeSerializer(posts, many=True)
        return Response({'results': data.data})


class UserTweets(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        tweets = profile.get_tweets()
        return Response({'results': tweets})


class UserYoutube(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        videos = profile.get_videos()
        return Response({'results': videos})


class UserPodcasts(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        source = request.GET.get('source')
        profile = request.user.profile
        podcasts = profile.get_podcasts(source=source)
        data = PodcastTimeSerializer(podcasts, many=True)
        return Response({'results': data.data})


class TechList(APIView):

    def get(self, request):
        techs = Tech.objects.all()
        data = TechSerializer(techs, many=True)
        return Response({'results': data.data})


class RegisterUser(APIView):

    def post(self, request):
        data = request.data
        email = data.get('email')
        password = data.get('password')
        fn = data.get('nickname')
        tech_ids = data.get('tech', [])

        if email and password and fn:
            try:
                user = User.objects.create_user(
                                    email=email, username=email,
                                    password=password, first_name=fn)
            except Exception as e:
                return Response({
                            'success': False,
                            'msg': 'email already registered'},
                            status=400)
        else:
            return Response({'msg': 'missing data', 'success': False},
                            status=400)

        profile = Profile.objects.create(user=user)
        tech = Tech.objects.filter(id__in=tech_ids)
        profile.tech.add(*list(tech))
        return Response({'success': True})


class ModifyUser(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request):
        user = request.user
        if not user.is_anonymous:
            data = request.data
            password = data.get('password')
            username = data.get('nickname')
            email = data.get('email')
            tech = data.get('tech')
            if password:
                user.set_password(password)
            if username:
                user.first_name = username
            if email:
                user.email = email
                user.username = email
            if tech:
                tech_ = Tech.objects.filter(id__in=tech)
                profile = user.profile
                profile.tech.clear()
                profile.tech.add(*list(tech_))
            user.save()
            return Response({'success': True})
        return Response({'success': False}, status=400)


class EmailAvailable(APIView):

    def get(self, request):
        email = request.GET.get('email')
        available = User.objects.filter(email=email).exists()
        return Response({'available': not available})


class UserProfile(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user_profile = request.user.profile
        data = UserProfileSerializer(user_profile)
        return Response({'user': data.data})


class UseRegisterToken(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        data = request.data
        user.profile.update_notifications(**data)
        return Response({'success': True})


class PostBookmark(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        posts = profile.get_posts_bookmarks()
        data = GenericPostSerializer(posts, many=True)
        return Response({'results': data.data})

    def post(self, request):
        user = request.user
        data = request.data
        post_id = data.get('post_id')
        success = user.profile.add_post_bookmark(post_id)
        return Response({'success': success})


class PostBookmarkDelete(APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, pk):
        user = request.user
        success = user.profile.delete_post_bookmark(pk)
        return Response({'success': success})


class PodcastBookmark(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        podcasts = profile.get_podcasts_bookmarks()
        data = GenericPodcastSerializer(podcasts, many=True)
        return Response({'results': data.data})

    def post(self, request):
        user = request.user
        data = request.data
        podcast_id = data.get('podcast_id')
        success = user.profile.add_podcast_bookmark(podcast_id)
        return Response({'success': success})


class PodcastBookmarkDelete(APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, pk):
        user = request.user
        success = user.profile.delete_podcast_bookmark(pk)
        return Response({'success': success})


class PostLike(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        posts = profile.get_posts_likes()
        data = GenericPostSerializer(posts, many=True)
        return Response({'results': data.data})

    def post(self, request):
        user = request.user
        data = request.data
        post_id = data.get('post_id')
        success = user.profile.add_post_like(post_id)
        return Response({'success': success})


class PodcastLike(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        podcasts = profile.get_podcasts_likes()
        data = GenericPodcastSerializer(podcasts, many=True)
        return Response({'results': data.data})

    def post(self, request):
        user = request.user
        data = request.data
        podcast_id = data.get('podcast_id')
        success = user.profile.add_podcast_like(podcast_id)
        return Response({'success': success})


class PodcastLikeDelete(APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, pk):
        user = request.user
        success = user.profile.delete_podcast_like(pk)
        return Response({'success': success})


class PostLikeDelete(APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, pk):
        user = request.user
        success = user.profile.delete_post_like(pk)
        return Response({'success': success})


class PostView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        posts = profile.get_posts_views()
        data = GenericPostSerializer(posts, many=True)
        return Response({'results': data.data})

    def post(self, request):
        user = request.user
        data = request.data
        post_id = data.get('post_id')
        success = user.profile.add_post_view(post_id)
        return Response({'success': success})


class PodcastView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = request.user.profile
        podcasts = profile.get_podcasts_views()
        data = GenericPodcastSerializer(podcasts, many=True)
        return Response({'results': data.data})

    def post(self, request):
        user = request.user
        data = request.data
        podcast_id = data.get('podcast_id')
        success = user.profile.add_podcast_view(podcast_id)
        return Response({'success': success})
