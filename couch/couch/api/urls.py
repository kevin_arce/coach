from django.conf.urls import url

from .views import (
    UserPosts, UserTweets, UserYoutube, RegisterUser,
    UserPodcasts, TechList, ModifyUser, EmailAvailable,
    UserProfile, UseRegisterToken, PostBookmark,
    PodcastBookmark, PostLike, PodcastLike, PostBookmarkDelete,
    PodcastBookmarkDelete, PodcastLikeDelete, PostLikeDelete,
    PodcastView, PostView
)

urlpatterns = [
    url(r'^tech/list/$', TechList.as_view(), name='tech_list'),

    url(r'^user/posts/$', UserPosts.as_view(), name='posts'),
    url(r'^user/tweets/$', UserTweets.as_view(), name='tweets'),
    url(r'^user/videos/$', UserYoutube.as_view(), name='videos'),
    url(r'^user/podcasts/$', UserPodcasts.as_view(), name='podcast'),
    url(r'^user/register/$', RegisterUser.as_view(), name='register_user'),
    url(r'^user/modify/$', ModifyUser.as_view(), name='modify_user'),
    url(r'^user/available-email/$', EmailAvailable.as_view(), name='avilable_email'),
    url(r'^user/profile/$', UserProfile.as_view(), name='profile'),
    url(r'^user/register-token/$', UseRegisterToken.as_view(), name='register_token'),
    url(r'^user/bookmarks/podcasts/$', PodcastBookmark.as_view(), name='podcast_bookmark'),
    url(r'^user/bookmarks/posts/$', PostBookmark.as_view(), name='post_bookmark'),

    url(r'^user/bookmarks/posts/(?P<pk>[\d]+)/$', PostBookmarkDelete.as_view(),
        name='post_bookmark_delete'),
    url(r'^user/bookmarks/podcasts/(?P<pk>[\d]+)/$', PodcastBookmarkDelete.as_view(),
        name='podcast_bookmark_delete'),

    url(r'^user/likes/podcasts/$', PodcastLike.as_view(), name='podcast_like'),
    url(r'^user/likes/posts/$', PostLike.as_view(), name='post_like'),

    url(r'^user/likes/podcasts/(?P<pk>[\d]+)/$', PodcastLikeDelete.as_view(),
        name='podcast_like_delete'),
    url(r'^user/likes/posts/(?P<pk>[\d]+)/$', PostLikeDelete.as_view(),
        name='post_like_delete'),

    url(r'^user/views/podcasts/$', PodcastView.as_view(), name='podcast_view'),
    url(r'^user/views/posts/$', PostView.as_view(), name='post_view'),

]
