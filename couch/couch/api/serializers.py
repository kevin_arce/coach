from couch.scrapers.models import Post, Tech
from couch.podcasts.models import Podcast
from couch.profiles.models import Profile

from rest_framework import serializers


class PostTimeSerializer(serializers.ModelSerializer):
    likes = serializers.IntegerField(source='count_likes')
    bookmarks = serializers.IntegerField(source='count_bookmarks')
    views = serializers.IntegerField(source='count_views')
    liked = serializers.BooleanField(source='likeado')
    bookmarked = serializers.BooleanField(source='bookmarkeado')
    viewed = serializers.BooleanField(source='viewido')

    class Meta:
        model = Post
        fields = ('id', 'date', 'title', 'url', 'source',
                  'summary', 'likes', 'bookmarks', 'liked',
                  'bookmarked', 'viewed', 'views')


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id', 'date', 'title', 'url', 'source', 'summary')


class PodcastTimeSerializer(serializers.ModelSerializer):
    likes = serializers.IntegerField(source='count_likes')
    bookmarks = serializers.IntegerField(source='count_bookmarks')
    views = serializers.IntegerField(source='count_views')
    liked = serializers.BooleanField(source='likeado')
    bookmarked = serializers.BooleanField(source='bookmarkeado')
    viewed = serializers.BooleanField(source='viewido')

    class Meta:
        model = Podcast
        fields = ('id', 'title', 'date', 'length', 'data_url',
                  'source', 'likes', 'bookmarks', 'liked',
                  'bookmarked', 'viewed', 'views')


class PodcastSerializer(serializers.ModelSerializer):

    class Meta:
        model = Podcast
        fields = ('id', 'title', 'date', 'length', 'data_url', 'source')


class TechSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tech
        fields = ('title', 'id')


class UserProfileSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(source='user.email')
    nickname = serializers.EmailField(source='user.first_name')
    tech = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ('email', 'nickname', 'tech')

    def get_tech(self, obj):
        return list(obj.tech.values('id', 'title'))


class GenericPostSerializer(serializers.Serializer):
    post = PostTimeSerializer()
    id = serializers.IntegerField()
    date = serializers.DateTimeField()


class GenericPodcastSerializer(serializers.Serializer):
    podcast = PodcastTimeSerializer()
    id = serializers.IntegerField()
    date = serializers.DateTimeField()
