from django.db import models


class ViewPost(models.Model):
    profile = models.ForeignKey('profiles.profile')
    post = models.ForeignKey('scrapers.post')
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('profile', 'post')


class ViewPodcast(models.Model):
    profile = models.ForeignKey('profiles.profile')
    podcast = models.ForeignKey('podcasts.podcast')
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('profile', 'podcast')
