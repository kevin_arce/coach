from django.contrib import admin
from .models import ViewPost, ViewPodcast

admin.site.register(ViewPodcast)
admin.site.register(ViewPost)
