from couch.scrapers.models import Post
from couch.podcasts.models import Podcast
from couch.utils.twitter.tweet_list import TwitterList
from couch.channels.models import YTChannel
from couch.bookmarks.models import BookmarkPost, BookmarkPodcast
from couch.likes.models import LikePost, LikePodcast
from couch.views.models import ViewPost, ViewPodcast

from django.db import models
from django.db.models import Count, Prefetch
from django.contrib.auth import get_user_model

import requests
from push_notifications.models import GCMDevice

User = get_user_model()


class Profile(models.Model):
    NOTIFICATION = (
        ('weekdays', 'weekdays'),
        ('weekend', 'weekend'),
        ('none', 'none')
    )

    user = models.OneToOneField(User, null=False)
    tech = models.ManyToManyField('scrapers.tech')
    notification_setting = models.CharField(choices=NOTIFICATION,
                                            blank=False,
                                            default='none',
                                            max_length=20)

    def get_posts(self, source=None):
        techs = self.tech.all()
        posts = Post.objects.get_with_all(tipe='post', pk=self.id)\
                            .filter(tech__in=techs).order_by('-date')\
                            .annotate(count_likes=Count('likepost'))\
                            .annotate(count_bookmarks=Count('bookmarkpost'))\
                            .annotate(count_views=Count('viewpost'))
        if source:
            posts = posts.filter(source=source)
        return posts

    def get_podcasts(self, source=None):
        techs = self.tech.all()
        podcasts = Podcast.objects.get_with_all(tipe='podcast', pk=self.id)\
                                  .filter(tech__in=techs).order_by('-date')\
                                  .annotate(count_likes=Count('likepodcast'))\
                                  .annotate(count_bookmarks=Count('bookmarkpodcast'))\
                                  .annotate(count_views=Count('viewpodcast'))
        if source:
            podcasts = podcasts.filter(source=source)
        return podcasts

    def get_tweets(self):
        techs = self.tech.all()
        api = TwitterList(tech=list(techs))
        tweets = api.get_tweets()
        return tweets

    def get_videos(self):
        techs = self.tech.all()
        channel = YTChannel.objects.filter(tech__in=techs).distinct('channel_id')
        response = []
        for i in channel:
            rq = requests.get('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={}&order=date&type=video&maxResults=50&key=AIzaSyAXB_Sude0Ngrohk-qlr4GPauN0LLI7Y50'.format(i.channel_id))
            json_ = rq.json()
            for i in json_['items']:
                response.append(
                    {
                        'title': i['snippet']['title'],
                        'image': i['snippet']['thumbnails']['high']['url'],
                        'url': 'https://www.youtube.com/watch?v='+i['id']['videoId'],
                        'source': i['snippet']['channelTitle'],
                        'date': i['snippet']['publishedAt']
                    })

        def by_date(item):
            return item['date']
        response = sorted(response, key=by_date, reverse=True)
        return response

    def update_notifications(self, **data):
        token = data.get('token')
        period = data.get('period')

        if token:
            exists = GCMDevice.objects.filter(registration_id=token)
            if not exists:
                GCMDevice.objects.create(user=self.user,
                                         registration_id=token,
                                         cloud_message_type='FCM')

        if period:
            self.notification_setting = period
            self.save()

    def add_post_bookmark(self, post_id):
        try:
            BookmarkPost.objects.create(profile=self, post_id=post_id)
        except Exception as e:
            print(e)
            return False
        else:
            return True

    def delete_post_bookmark(self, post_id):
        post = BookmarkPost.objects.filter(profile=self, post_id=post_id)
        if post.exists():
            post.delete()
            return True
        return False

    def add_podcast_bookmark(self, podcast_id):
        try:
            BookmarkPodcast.objects.create(profile=self, podcast_id=podcast_id)
        except Exception as e:
            print(e)
            return False
        else:
            return True

    def delete_podcast_bookmark(self, podcast_id):
        podcast = BookmarkPodcast.objects.filter(profile=self,
                                                 podcast_id=podcast_id)
        if podcast.exists():
            podcast.delete()
            return True
        return False

    def add_post_like(self, post_id):
        try:
            LikePost.objects.create(profile=self, post_id=post_id)
        except Exception as e:
            print(e)
            return False
        else:
            return True

    def delete_post_like(self, post_id):
        post = LikePost.objects.filter(profile=self, post_id=post_id)
        if post.exists():
            post.delete()
            return True
        return False

    def add_podcast_like(self, podcast_id):
        try:
            LikePodcast.objects.create(profile=self, podcast_id=podcast_id)
        except Exception as e:
            print(e)
            return False
        else:
            return True

    def delete_podcast_like(self, podcast_id):
        podcast = LikePodcast.objects.filter(profile=self,
                                             podcast_id=podcast_id)
        if podcast.exists():
            podcast.delete()
            return True
        return False

    def add_post_view(self, post_id):
        try:
            ViewPost.objects.create(profile=self, post_id=post_id)
        except Exception as e:
            print(e)
            return False
        else:
            return True

    def add_podcast_view(self, podcast_id):
        try:
            ViewPodcast.objects.create(profile=self, podcast_id=podcast_id)
        except Exception as e:
            print(e)
            return False
        else:
            return True

    def get_posts_likes(self):
        return LikePost.objects.filter(profile=self).prefetch_related(
                    Prefetch('post',
                             queryset=Post.objects.get_with_all(tipe='post', pk=self.id))
                    )

    def get_podcasts_likes(self):
        return LikePodcast.objects.filter(profile=self).prefetch_related(
                    Prefetch('podcast',
                             queryset=Podcast.objects.get_with_all(tipe='podcast', pk=self.id))
                    )

    def get_posts_bookmarks(self):
        return BookmarkPost.objects.filter(profile=self).prefetch_related(
                    Prefetch('post',
                             queryset=Post.objects.get_with_all(tipe='post', pk=self.id))
                    )

    def get_podcasts_bookmarks(self):
        return BookmarkPodcast.objects.filter(profile=self).prefetch_related(
                    Prefetch('podcast',
                             queryset=Podcast.objects.get_with_all(tipe='podcast', pk=self.id))
                    )

    def get_posts_views(self):
        return ViewPost.objects.filter(profile=self)

    def get_podcasts_views(self):
        return ViewPodcast.objects.filter(profile=self)
