# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-22 21:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='notification_setting',
            field=models.CharField(choices=[('weekdays', 'weekdays'), ('weekend', 'weekend'), ('none', 'none')], default='none', max_length=20),
        ),
    ]
