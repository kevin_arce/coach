from django.db import models


class YTChannel(models.Model):
    channel_id = models.CharField(max_length=200, blank=False)
    tech = models.ForeignKey('scrapers.tech', null=False)

    def __str__(self):
        return "{} - {}".format(self.channel_id, self.tech)
