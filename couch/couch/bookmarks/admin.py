from django.contrib import admin
from .models import BookmarkPost, BookmarkPodcast

admin.site.register(BookmarkPodcast)
admin.site.register(BookmarkPost)
