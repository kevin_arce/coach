from django.db import models
from couch.managers.managers import FullDataManager


class BookmarkPost(models.Model):
    profile = models.ForeignKey('profiles.profile')
    post = models.ForeignKey('scrapers.post')
    date = models.DateTimeField(auto_now_add=True)
    objects = FullDataManager()

    class Meta:
        unique_together = ('profile', 'post')


class BookmarkPodcast(models.Model):
    profile = models.ForeignKey('profiles.profile')
    podcast = models.ForeignKey('podcasts.podcast')
    date = models.DateTimeField(auto_now_add=True)
    objects = FullDataManager()

    class Meta:
        unique_together = ('profile', 'podcast')
