from django.db import models
from couch.managers.managers import FullDataManager


class LikePost(models.Model):
    profile = models.ForeignKey('profiles.profile')
    post = models.ForeignKey('scrapers.post')
    date = models.DateTimeField(auto_now_add=True)
    objects = FullDataManager()

    class Meta:
        unique_together = ('profile', 'post')


class LikePodcast(models.Model):
    profile = models.ForeignKey('profiles.profile')
    podcast = models.ForeignKey('podcasts.podcast')
    date = models.DateTimeField(auto_now_add=True)
    objects = FullDataManager()

    class Meta:
        unique_together = ('profile', 'podcast')
