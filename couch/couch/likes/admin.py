from django.contrib import admin
from .models import LikePost, LikePodcast

admin.site.register(LikePost)
admin.site.register(LikePodcast)
