from django.db import models
from django.db.models import Count


class FullDataManager(models.Manager):

    def get_with_all(self, tipe, pk, extra=None):
        if extra:
            last = extra
        else:
            last = 'id'

        table = self.model._meta.db_table
        return super(FullDataManager, self).get_queryset()\
                        .extra(select={
                            'likeado': '''
                                SELECT exists(SELECT 1 from
                                likes_like{}
                                where profile_id=%s
                                and {}_id={}.{} limit 1)
                                '''.format(tipe, tipe, table, last)},
                               select_params=(pk,))\
                        .extra(select={
                            'bookmarkeado': '''
                                SELECT exists(SELECT 1 from
                                bookmarks_bookmark{}
                                where profile_id=%s
                                and {}_id={}.{} limit 1)
                                '''.format(tipe, tipe, table, last)},
                                select_params=(pk,))\
                        .extra(select={
                            'viewido': '''
                                SELECT exists(SELECT 1 from
                                views_view{}
                                where profile_id=%s
                                and {}_id={}.{} limit 1)
                                '''.format(tipe, tipe, table, last)},
                                select_params=(pk,))\
                        .annotate(count_likes=Count('like{}'.format(tipe)))\
                        .annotate(count_bookmarks=Count('bookmark{}'.format(tipe)))\
                        .annotate(count_views=Count('view{}'.format(tipe)))
