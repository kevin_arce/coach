from django.db import models


class TWList(models.Model):
    list_id = models.CharField(max_length=50, blank=False)
    tech = models.OneToOneField('scrapers.tech', null=True)

    def __str__(self):
        return "{}-{}".format(self.list_id, self.tech)
