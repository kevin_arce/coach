from .android.adapter import AndroidScraperAdapter
from .python.adapter import PythonScraperAdapter
from .react_native.adapter import ReactNativeScraperAdapter
from .react.adapter import ReactScraperAdapter
from .ruby.adapter import RubyScraperAdapter

from ..variables import (
    ANDROID, PYTHON, REACT_NATIVE, REACT,
    ROR,
)


class ScrapAdapter(object):

    def __init__(self, url, tech):
        self.url = url
        self.tech = tech

    def get_scrap_adapter(self):
        if self.tech == ANDROID:
            return AndroidScraperAdapter(self.url)
        elif self.tech == PYTHON:
            return PythonScraperAdapter(self.url)
        elif self.tech == REACT_NATIVE:
            return ReactNativeScraperAdapter(self.url)
        elif self.tech == REACT:
            return ReactScraperAdapter(self.url)
        elif self.tech == ROR:
            return RubyScraperAdapter(self.url)

    def scrap(self):
        scraper = self.get_scrap_adapter()
        if scraper:
            scraper.scrap()
