from . import variables
from .scrapers import (
    GoogleBlog, StylingAndroid, AndroidPub, AndroidUIPatterns
)

variables = variables.Variables()


class AndroidScraperAdapter(object):

    def __init__(self, url, unique_target=True):
        self.url = url
        self.unique_target = unique_target

    def get_scraper(self):
        if self.unique_target:
            if variables.GOOGLE_BLOG in self.url:
                return GoogleBlog(url=self.url)
            elif variables.STYLING_DR in self.url:
                return StylingAndroid(url=self.url)
            elif variables.ANDROID_PUB in self.url:
                return AndroidPub(url=self.url)
            elif variables.ANDROID_UI in self.url:
                return AndroidUIPatterns(url=self.url)

    def scrap(self):
        scraper = self.get_scraper()
        scraper.do_it()
