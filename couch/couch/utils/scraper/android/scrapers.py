from datetime import datetime

import requests
from bs4 import BeautifulSoup

from couch.scrapers.models import Post, Tech
from ...variables import ANDROID
from ... import utils


class GoogleBlog(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'post'})
        tech = Tech.objects.get(short_title=ANDROID)
        post_list = []
        for i in posts:
            title_obj = i.find('h2')
            title = title_obj.get_text()
            url = title_obj.find('a')['href']
            date_text = i.find('span', {'class': 'publishdate'}).get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%d%B%Y")
            summary_ = i.find_all('p')
            summary = ''
            for j in summary_:
                summary = j.get_text()
                if summary:
                    break

            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Google Blog'))
        Post.objects.bulk_create(post_list)


class StylingAndroid(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('article')
        post_list = []
        tech = Tech.objects.get(short_title=ANDROID)
        for post in posts:
            title_obj = post.find('h2')
            title = title_obj.get_text()
            url = title_obj.find('a')['href']
            datetime_ = post.find('time')['datetime'][:10]
            date = datetime.strptime(datetime_, "%Y-%m-%d")
            summary = post.find('p').get_text()
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Styling Android'))
        Post.objects.bulk_create(post_list)


class AndroidPub(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'js-trackedPost'})
        post_list = []
        tech = Tech.objects.get(short_title=ANDROID)
        for post in posts:
            title_obj = post.find('h3')
            if not title_obj:
                continue

            title = title_obj.get_text().encode('ascii', 'ignore').decode()
            url = post.find('a')['href']
            summary = post.find('div', {'class': 'u-baseColor--textNormal'}).get_text()
            summary = summary.encode('ascii', 'ignore').decode()
            datetime_ = post.find('time')['datetime'][:10]
            date = datetime.strptime(datetime_, "%Y-%m-%d")

            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Android pub'))
        Post.objects.bulk_create(post_list)


class AndroidUIPatterns(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'js-trackedPost'})
        post_list = []
        tech = Tech.objects.get(short_title=ANDROID)
        for post in posts:
            try:
                title_obj = post.find('h3')
                if not title_obj:
                    continue

                title = title_obj.get_text().encode('ascii', 'ignore').decode()
                url = post.find('a')['href']
                summary = post.find('p').get_text()
                summary = summary.encode('ascii', 'ignore').decode()
                datetime_ = post.find('time')['datetime'][:10]
                date = datetime.strptime(datetime_, "%Y-%m-%d")

                normalize_title = utils.remove_lines_breaks(title)
                if not (Post.objects.filter(title=normalize_title).exists()):
                    post_list.append(Post(title=normalize_title,
                                          url=url, tech=tech, date=date,
                                          summary=utils.remove_lines_breaks(summary),
                                          source='Android UI Patterns'))
            except:
                pass
        Post.objects.bulk_create(post_list)
