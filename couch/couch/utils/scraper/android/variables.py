
class Variables(object):

    def __init__(self):
        self.GOOGLE_BLOG = 'https://android-developers.googleblog.com'
        self.STYLING_DR = 'https://blog.stylingandroid.com'
        self.ANDROID_PUB = 'https://android.jlelse.eu'
        self.ANDROID_UI = 'https://androiduipatterns.com'
