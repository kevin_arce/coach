from . import variables
from .scrapers import (
    ReactBlog, ReactNews,
)

variables = variables.Variables()


class ReactScraperAdapter(object):

    def __init__(self, url, unique_target=True):
        self.url = url
        self.unique_target = unique_target

    def get_scraper(self):
        if self.unique_target:
            if variables.REACT_BLOG in self.url:
                return ReactBlog(url=self.url)
            elif variables.REACT_NEWS in self.url:
                return ReactNews(url=self.url)

    def scrap(self):
        scraper = self.get_scraper()
        if scraper:
            scraper.do_it()
