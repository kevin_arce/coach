from datetime import datetime

import feedparser
import html as htmlparser
import requests
from bs4 import BeautifulSoup

from couch.scrapers.models import Post, Tech
from ...variables import REACT
from ... import utils


class ReactNews(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        parser = feedparser.parse(self.url)
        post_list = []
        tech = Tech.objects.get(short_title=REACT)
        source = parser.channel.title
        for post in parser.entries:
            date = datetime(*post.published_parsed[:6]).date()
            title = post.title

            try:
                if '<' in post.summary:
                    html = BeautifulSoup(post.summary, 'lxml')
                    try:
                        summary = htmlparser.unescape(
                                        html.find('p').get_text().split('.')[0])
                    except:
                        summary = ''
                else:
                    summary = post.summary
            except AttributeError:
                summary = ''

            url = post.link
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source=source))
        Post.objects.bulk_create(post_list)


class ReactBlog(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('li', {'class': 'css-a5dudd'})
        tech = Tech.objects.get(short_title=REACT)
        post_list = []
        for i in posts:
            title_obj = i.find('h2')
            title = title_obj.get_text()
            url_ = title_obj.find('a')['href']
            url = 'https://reactjs.org{}'.format(url_)
            date_text = i.find('div').get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%B%d,%Y")
            summary = ''
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='ReactJS Blog'))
        Post.objects.bulk_create(post_list)
