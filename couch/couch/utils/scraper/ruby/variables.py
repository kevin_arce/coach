class Variables(object):

    def __init__(self):
        self.JETBRAINS = 'https://blog.jetbrains.com/ruby'
        self.MIKE_PERHAM = 'http://www.mikeperham.com'
        self.ARKENCY = 'http://blog.arkency.com'
        self.RUBY_PIGEON = 'https://www.rubypigeon.com'
        self.DRIFTING_RUBY = 'https://blog.driftingruby.com'
