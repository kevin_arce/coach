from . import variables
from .scrapers import (
    JetBrains, MikePerham, ArkencyBlog,
    DriftingRuby, RubyPigeon,
)

variables = variables.Variables()


class RubyScraperAdapter(object):

    def __init__(self, url, unique_target=True):
        self.url = url
        self.unique_target = unique_target

    def get_scraper(self):
        if self.unique_target:
            if variables.JETBRAINS in self.url:
                return JetBrains(url=self.url)
            elif variables.MIKE_PERHAM in self.url:
                return MikePerham(url=self.url)
            elif variables.ARKENCY in self.url:
                return ArkencyBlog(url=self.url)
            elif variables.DRIFTING_RUBY in self.url:
                return DriftingRuby(url=self.url)
            elif variables.RUBY_PIGEON in self.url:
                return RubyPigeon(url=self.url)

    def scrap(self):
        scraper = self.get_scraper()
        if scraper:
            scraper.do_it()
