from datetime import datetime

import requests
from bs4 import BeautifulSoup

from couch.scrapers.models import Post, Tech
from ...variables import ROR
from ... import utils


class JetBrains(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'post'})
        tech = Tech.objects.get(short_title=ROR)
        post_list = []
        for i in posts:
            title_obj = i.find('h2')
            title = title_obj.get_text()
            url = title_obj.find('a')['href']
            date_text = i.find('span', {'class': 'entry-date'}).get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%B%d,%Y")
            summary = i.find('div', {'class': 'entry-content'}).get_text()
            summary = utils.remove_lines_breaks(summary)
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='JetBrains'))
        Post.objects.bulk_create(post_list)


class MikePerham(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('article', {'class': 'post'})
        tech = Tech.objects.get(short_title=ROR)
        post_list = []
        for i in posts:
            title_obj = i.find('a')
            title = title_obj.get_text()
            url = title_obj['href']
            date_text = i.find('span').get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%Y-%m-%d")
            summary = ''
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Mike Perham'))
        Post.objects.bulk_create(post_list)


class ArkencyBlog(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'article-list__article'})
        tech = Tech.objects.get(short_title=ROR)
        post_list = []
        for i in posts:
            title_obj = i.find('a')
            title = title_obj.get_text()
            url_ = title_obj['href']
            url = 'https://blog.arkency.com{}'.format(url_)
            date_text = i.find('time')['datetime'][:10]
            date = datetime.strptime(utils.normalize_text(date_text), "%Y-%m-%d")
            summary = ''
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Arkency Blog'))
        Post.objects.bulk_create(post_list)


class DriftingRuby(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'post'})
        tech = Tech.objects.get(short_title=ROR)
        post_list = []
        for i in posts:
            title_obj = i.find('h2')
            title = title_obj.get_text()
            url = title_obj.find('a')['href']
            date_text = i.find('time', {'class': 'entry-date'}).get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%B%d,%Y")
            summary = i.find('div', {'class': 'entry-content'}).get_text()
            summary = utils.remove_lines_breaks(summary)
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Drifting Ruby'))
        Post.objects.bulk_create(post_list)


class RubyPigeon(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('li')
        tech = Tech.objects.get(short_title=ROR)
        post_list = []
        for i in posts[:-3]:
            title_obj = i.find('a')
            title = title_obj.get_text()
            url = title_obj['href']
            date_text = i.find('p').get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%b%d,%Y")
            summary = ''
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='RubyPigeon'))
        Post.objects.bulk_create(post_list)
