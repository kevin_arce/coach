from datetime import datetime

import requests
from bs4 import BeautifulSoup

from couch.scrapers.models import Post, Tech
from ...variables import REACT_NATIVE
from ... import utils


class ReactBlog(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('article', {'class': 'entry-excerpt'})
        tech = Tech.objects.get(short_title=REACT_NATIVE)
        post_list = []
        for i in posts:
            title_obj = i.find('h1')
            title = title_obj.get_text()
            url = title_obj.find('a')['href']
            date_text = i.find('time').get_text()
            date = datetime.strptime(utils.normalize_text(date_text), "%B%d,%Y")
            summary = i.find('p').get_text()
            url = 'https://facebook.github.io{}'.format(url)

            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='React Native Blog'))
        Post.objects.bulk_create(post_list)


class CallStack(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('div', {'class': 'streamItem'})
        tech = Tech.objects.get(short_title=REACT_NATIVE)
        post_list = []
        for i in posts:
            title_obj = i.find('h3')
            title = title_obj.get_text()
            url = i.find_all('a')[-1]['href']
            datetime_ = i.find('time')['datetime'][:10]
            date = datetime.strptime(datetime_, "%Y-%m-%d")
            summary = i.find('h4')
            if not summary:
                summary = i.find('p')
            if summary:
                summary = summary.get_text()
            else:
                summary = ''

            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source='Callstack'))
        Post.objects.bulk_create(post_list)
