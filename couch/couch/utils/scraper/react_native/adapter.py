from . import variables
from .scrapers import (
    ReactBlog, CallStack,
)

variables = variables.Variables()


class ReactNativeScraperAdapter(object):

    def __init__(self, url, unique_target=True):
        self.url = url
        self.unique_target = unique_target

    def get_scraper(self):
        if self.unique_target:
            if variables.REACT_NATIVE_BLOG in self.url:
                return ReactBlog(url=self.url)
            elif variables.CALL_STACK in self.url:
                return CallStack(url=self.url)

    def scrap(self):
        scraper = self.get_scraper()
        scraper.do_it()
