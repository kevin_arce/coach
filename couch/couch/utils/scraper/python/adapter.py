from .scrapers import (
    PlanetPython
)


class PythonScraperAdapter(object):

    def __init__(self, url, unique_target=True):
        self.url = url
        self.unique_target = unique_target

    def get_scraper(self):
        return PlanetPython(url=self.url)

    def scrap(self):
        scraper = self.get_scraper()
        scraper.do_it()
