from datetime import datetime

import requests
import feedparser
import html as htmlparser
from bs4 import BeautifulSoup

from couch.scrapers.models import Post, Tech
from ...variables import PYTHON
from ... import utils


class PlanetPythonOld(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        rq = requests.get(self.url)
        html = BeautifulSoup(rq.content, "lxml")
        posts = html.find_all('h3', {'class': 'post'})
        tech = Tech.objects.get(short_title=PYTHON)
        post_list = []
        date_text = ''
        for post in posts:
            source = post.find('a')['title']
            title_obj = post.findNext('h4')
            title = title_obj.get_text()
            url = title_obj.find('a')['href']

            if post != posts[-1]:
                hr = post.findNext('hr')
                datetime_ = hr.findPrevious('em')
            else:
                datetime_ = post.findAllNext('em')[-1]
            if datetime_:
                date_text = utils.normalize_text(datetime_.get_text())[:-7]
            if date_text:
                date = datetime.strptime(date_text, "%B%d,%Y")
            else:
                date = datetime.now()
            summary = title_obj.findAllNext('p')[1].get_text()

            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source=source))
        Post.objects.bulk_create(post_list)


class PlanetPython(object):

    def __init__(self, url):
        self.url = url

    def do_it(self):
        parser = feedparser.parse(self.url)
        post_list = []
        tech = Tech.objects.get(short_title=PYTHON)
        for post in parser.entries:
            date = datetime(*post.published_parsed[:6]).date()
            title_obj = post.title.split(':')
            title = title_obj[1].strip()
            source = title_obj[0].strip()

            try:
                if '<' in post.summary:
                    html = BeautifulSoup(post.summary, 'lxml')
                    try:
                        summary = html.find('p').get_text().split('.')[0]
                    except:
                        summary = ''
                else:
                    summary = post.summary
            except AttributeError:
                summary = ''

            url = post.link
            normalize_title = utils.remove_lines_breaks(title)
            if not (Post.objects.filter(title=normalize_title).exists()):
                post_list.append(Post(title=normalize_title,
                                      url=url, tech=tech, date=date,
                                      summary=utils.remove_lines_breaks(summary),
                                      source=source))
        Post.objects.bulk_create(post_list)
