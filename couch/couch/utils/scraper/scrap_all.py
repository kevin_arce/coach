import importlib

from couch.utils.variables import MAPER
from couch.scrapers.models import Tech, Post
from couch.tasks.models import TaskLog

from .adapter import ScrapAdapter


def scrap_all():
    techs = Tech.objects.all()
    for tech in techs:
        try:
            last_count = Post.objects.filter(tech=tech).count()
            folder = MAPER[tech.short_title]
            base = 'couch.utils.scraper.{}.variables'.format(folder)
            variables = importlib.import_module(base)
            vars_ = variables.Variables()
            for key, url in vars_.__dict__.items():
                ScrapAdapter(tech=tech.short_title, url=url).scrap()
            new_count = Post.objects.filter(tech=tech).count()

            # creates the new log with the count and date
            difference = new_count - last_count
            TaskLog.objects.create(count=difference, tech=tech, data_type='post')
        except KeyError:
            pass
