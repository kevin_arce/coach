def remove_lines_breaks(text):
    """
    function to remove '\n' char
    """
    text = text.replace('\n', '')
    return text


def remove_spaces(text):
    """
    functions to remove spaces
    """
    text = text.replace(' ', '')
    return text


def normalize_text(text):
    """
    functions to remove spaces and lines
    """
    text = remove_spaces(remove_lines_breaks(text))
    return text
