ANDROID = 'droid'
PYTHON = 'py'
SWIFT = 'swift'
OBJECTIVE_C = 'objective-c'
KOTLIN = 'kotlin'
IONIC = 'ionic'
XAMARIN = 'xamarin'
REACT_NATIVE = 'react-native'
DJANGO = 'dj'
FLASK = 'flask'
ROR = 'ror'
NODE_JS = 'node'
ANGULAR = 'angular'
REACT = 'react'
RUBY = 'ruby'

OPTIONS = (
    (ANDROID, ANDROID),
    (PYTHON, PYTHON),
    (SWIFT, SWIFT),
    (OBJECTIVE_C, OBJECTIVE_C),
    (KOTLIN, KOTLIN),
    (IONIC, IONIC),
    (XAMARIN, XAMARIN),
    (REACT_NATIVE, REACT_NATIVE),
    (DJANGO, DJANGO),
    (FLASK, FLASK),
    (ROR, ROR),
    (NODE_JS, NODE_JS),
    (ANGULAR, ANGULAR),
    (REACT, REACT),
)


MAPER = {
    ANDROID: 'android',
    PYTHON: 'python',
    REACT_NATIVE: 'react_native',
    REACT: 'react',
    ROR: 'ruby',
}
