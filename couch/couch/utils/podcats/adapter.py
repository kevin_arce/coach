from couch.podcasts.models import Podcast

import feedparser
from datetime import datetime


class FeedAdapter(object):

    def __init__(self, url, tech):
        self.url = url
        self.tech = tech

    def save_to_bd(self):
        parse = feedparser.parse(self.url)
        obj_list = []
        podcasts_created = list(Podcast.objects.values_list('title', flat=True))

        for entry in parse.entries:
            try:
                link = ""
                links = entry.links
                for i in links:
                    if 'audio' in i.get('type', ''):
                        link = i['href']

                if hasattr(entry, 'itunes_duration'):
                    length = entry.itunes_duration
                else:
                    length = 0
                date = datetime(*entry.published_parsed[:6]).date()

                if entry.title not in podcasts_created:
                    obj_list.append(Podcast(title=entry.title,
                                            length=length,
                                            date=date,
                                            tech=self.tech,
                                            data_url=link,
                                            source=parse.channel.title
                                            ))
            except Exception as e:
                print(e)
                continue
        Podcast.objects.bulk_create(obj_list)
