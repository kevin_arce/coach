from couch.podcasts.models import PodcastURL
from utils.podcats.adapter import FeedAdapter


def update_podcasts():
    all_podcasts = PodcastURL.objects.all()
    for podcast in all_podcasts:
        FeedAdapter(url=podcast.url, tech=podcast.tech).save_to_bd()
