from couch.lists.models import TWList

from .api import TwitterAPI


class TwitterList(object):

    def __init__(self, tech):
        self.tech = tech
        self.api = TwitterAPI().connect()

    def get_tech_list(self):
        if isinstance(self.tech, list):
            tl = TWList.objects.filter(tech__in=self.tech)\
                        .values_list('list_id', flat=True)
            return list(tl)
        return TWList.objects.get(tech=self.tech).list_id

    def __get_tweets(self):
        list_id = self.get_tech_list()
        data = []
        if isinstance(list_id, list):
            for item in list_id:
                data += self.api.GetListTimeline(list_id=item, count=100)

            def by_date(item):
                return item.created_at
            data = sorted(data, key=by_date, reverse=True)
        else:
            data = self.api.GetListTimeline(list_id=list_id, count=100)
        return data

    def get_tweets(self):
        data = self.__get_tweets()
        response = []
        for tweet in data:
            if not tweet.in_reply_to_screen_name:
                _media = tweet.media
                _urls = tweet.urls

                media = []
                urls = []

                if _media:
                    for x in _media:
                        media.append({
                            'url': x.media_url,
                            'type': x.type,
                            'video': x.video_info})
                if _urls:
                    for url in _urls:
                        urls.append(url.url)

                response.append({
                    'username': tweet.user.screen_name,
                    'avatar': tweet.user.profile_image_url,
                    'text': tweet.text,
                    'media': media,
                    'urls': urls,
                    'date': tweet.created_at})

        return response
