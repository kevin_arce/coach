from django.conf import settings

import twitter


class TwitterAPI(object):

    def __init__(self):
        self.api = None

    def connect(self):
        if not self.api:
            self.api = twitter.Api(
                        consumer_key=settings.TWITTER_CONSUMER_KEY,
                        consumer_secret=settings.TWITTER_CONSUMER_SECRET,
                        access_token_key=settings.TWITTER_ACCESS_KEY,
                        access_token_secret=settings.TWITTER_ACCESS_SECRET)
        return self.api
