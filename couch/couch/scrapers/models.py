from django.db import models
from couch.utils.variables import OPTIONS
from couch.managers.managers import FullDataManager


class Tech(models.Model):
    title = models.CharField(max_length=200)
    short_title = models.CharField(max_length=50, choices=OPTIONS, unique=True)

    def __str__(self):
        return self.title


class Post(models.Model):
    url = models.TextField(blank=False)
    date = models.DateField(null=False, auto_now=False)
    title = models.TextField(blank=False)
    tech = models.ForeignKey('Tech')
    summary = models.TextField(blank=True)
    source = models.CharField(max_length=150, blank=True)
    objects = FullDataManager()

    def __str__(self):
        return self.title
