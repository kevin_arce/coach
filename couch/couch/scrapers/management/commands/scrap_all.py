from datetime import datetime

from django.core.management.base import BaseCommand
from couch.utils.scraper import scrap_all


class Command(BaseCommand):

    def handle(self, *args, **options):
        scrap_all.scrap_all()
        print('succeed at:', datetime.now())
