from django.contrib import admin

from .models import Tech, Post

admin.site.register(Tech)
admin.site.register(Post)
